# GitLab CI run job after manual trigger

See the main issue for a description: [#31264](https://gitlab.com/gitlab-org/gitlab/-/issues/31264)

## Possible Strategies

- **Add job after manual job** (Failed)<br />
  Pipeline is in `running` state until the manual job gets triggered ([#17768](https://gitlab.com/gitlab-org/gitlab/-/issues/17768)).

- **Use child pipelines** (Failed)<br />
  Prevented by using `when:manual` together with `trigger:` ([#201938](https://gitlab.com/gitlab-org/gitlab/-/issues/201938)).

- **Use parallel `matrix` jobs** (Failed)<br />
  Creates multiple manual jobs which have to be triggered all manually.
